<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Enum;

use MabeEnum\Enum;

final class Type extends Enum
{
    public const PASSWORD_CHANGE = 0;
    public const PASSWORD_RESTORE = 1;
}
