<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Enum\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type as AbstractType;
use Sky\Component\Confirmation\Enum\Type;

class TypeType extends AbstractType
{
    private const NAME = 'confirmation_type';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getSmallIntTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Type
    {
        if (null !== $value) {
            return Type::byValue((int) $value);
        }

        return null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        /* @var Type $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
