<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Repository\Doctrine;

use Doctrine\Common\Persistence\ManagerRegistry;
use Sky\Component\Confirmation\Exception\ConfirmationNotFoundException;
use Sky\Component\Confirmation\Model\Confirmation;
use Sky\Component\Confirmation\Repository\ConfirmationRepositoryInterface;
use Sky\Component\Core\Repository\Doctrine\BaseRepository;

class ConfirmationRepository extends BaseRepository implements ConfirmationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Confirmation::class);
    }

    public function getNotExpiredByCode(string $code): Confirmation
    {
        $qb = $this->createQueryBuilder('c');

        $model = $qb->select('c')
            ->where('c.code = :code')
            ->andWhere('c.expireAt < :now')
            ->setParameter('code', $code)
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $model) {
            throw ConfirmationNotFoundException::missingNotExpiredByCode($code);
        }

        return $model;
    }
}
