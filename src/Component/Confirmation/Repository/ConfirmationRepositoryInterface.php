<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Repository;

use Sky\Component\Confirmation\Model\Confirmation;
use Sky\Component\Core\Repository\RepositoryInterface;

interface ConfirmationRepositoryInterface extends RepositoryInterface
{
    public function getNotExpiredByCode(string $code): Confirmation;
}
