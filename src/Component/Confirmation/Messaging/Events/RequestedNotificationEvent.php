<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Messaging\Events;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\Notification\Enum\Channel;

class RequestedNotificationEvent
{
    private $id;
    private $channel;

    public function __construct(UuidInterface $id, Channel $channel)
    {
        $this->id = $id;
        $this->channel = $channel->getValue();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function channel(): Channel
    {
        return Channel::byValue($this->channel);
    }
}
