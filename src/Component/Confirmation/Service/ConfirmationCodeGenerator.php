<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Service;

use Sky\Component\Confirmation\Exception\ConfirmationNotFoundException;
use Sky\Component\Confirmation\Repository\ConfirmationRepositoryInterface;
use Sky\Component\Core\CodeGenerator\GeneratorInterface;

class ConfirmationCodeGenerator
{
    public const LENGTH = 8;

    private $confirmationRepository;
    private $codeGenerator;

    public function __construct(ConfirmationRepositoryInterface $confirmationRepository, GeneratorInterface $codeGenerator)
    {
        $this->confirmationRepository = $confirmationRepository;
        $this->codeGenerator = $codeGenerator;
    }

    public function generate(): string
    {
        foreach ($this->codeGenerator->generate(self::LENGTH) as $code) {
            try {
                $this->confirmationRepository->getNotExpiredByCode($code);
            } catch (ConfirmationNotFoundException $e) {
                return $code;
            }
        }
    }
}
