<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Service;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\Confirmation\Configuration\ConfirmationConfiguration;
use Sky\Component\Confirmation\Enum\Type;
use Sky\Component\Confirmation\Model\Builder\ConfirmationBuilder;
use Sky\Component\Confirmation\Model\Confirmation;
use Sky\Component\Confirmation\Repository\ConfirmationRepositoryInterface;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Model\Notification;
use Sky\Component\User\Model\User;

class ConfirmationService
{
    private $confirmationRepository;
    private $codeGenerator;
    private $configuration;

    public function __construct(
        ConfirmationRepositoryInterface $confirmationRepository,
        ConfirmationCodeGenerator $codeGenerator,
        ConfirmationConfiguration $configuration
    ) {
        $this->confirmationRepository = $confirmationRepository;
        $this->codeGenerator = $codeGenerator;
        $this->configuration = $configuration;
    }

    public function createResetPasswordConfirmation(User $user, Channel $channel): Confirmation
    {
        $confirmation = $this->create($user, $this->configuration->resetPasswordTtl())
            ->withType(Type::byValue(Type::PASSWORD_RESTORE))
            ->build();

        $confirmation->requestNotificationViaChannel($channel);

        $this->confirmationRepository->save($confirmation);

        return $confirmation;
    }

    public function assignConfirmationNotification(Confirmation $confirmation, Notification $notification): void
    {
        $confirmation->assignNotification($notification);

        $this->confirmationRepository->save($confirmation);
    }

    public function getById(UuidInterface $id): Confirmation
    {
        return $this->confirmationRepository->getById($id);
    }

    private function create(User $user, int $ttl): ConfirmationBuilder
    {
        $code = $this->codeGenerator->generate();

        return (new ConfirmationBuilder())
            ->withId($this->confirmationRepository->nextIdentity())
            ->withUser($user)
            ->withCode($code)
            ->withTtl($ttl);
    }
}
