<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Configuration;

class ConfirmationConfiguration
{
    private $resetPasswordTtl;

    public function __construct(
        int $resetPasswordTtl
    ) {
        $this->resetPasswordTtl = $resetPasswordTtl;
    }

    public function resetPasswordTtl(): int
    {
        return $this->resetPasswordTtl;
    }
}
