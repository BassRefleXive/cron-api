<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Exception;

final class ConfirmationNotFoundException extends \RuntimeException
{
    public static function missingNotExpiredByCode(string $code): self
    {
        return new self(
            sprintf(
                'Not expired confirmation with code "%s" not found.',
                $code
            )
        );
    }
}
