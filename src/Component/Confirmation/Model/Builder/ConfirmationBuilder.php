<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Model\Builder;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\Confirmation\Enum\Type;
use Sky\Component\Confirmation\Model\Confirmation;
use Sky\Component\Core\Exception\BuilderException;
use Sky\Component\Core\Model\BuildableInterface;
use Sky\Component\Core\Model\Builder\BuilderInterface;
use Sky\Component\User\Model\User;

class ConfirmationBuilder implements BuilderInterface
{
    private $id;
    private $user;
    private $code;
    private $type;
    private $ttl;

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function withId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function user(): User
    {
        return $this->user;
    }

    public function withUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function withCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function type(): Type
    {
        return $this->type;
    }

    public function withType(Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function ttl(): int
    {
        return $this->ttl;
    }

    public function withTtl(int $ttl): self
    {
        $this->ttl = $ttl;

        return $this;
    }

    /**
     * @return BuildableInterface|Confirmation
     */
    public function build(): BuildableInterface
    {
        $this->validate();

        return new Confirmation($this);
    }

    private function validate(): void
    {
        foreach ($this as $property => $value) {
            if (null === $value) {
                throw BuilderException::missingPropertyValue($property, Confirmation::class);
            }
        }
    }
}
