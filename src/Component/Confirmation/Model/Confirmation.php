<?php

declare(strict_types=1);

namespace Sky\Component\Confirmation\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\UuidInterface;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;
use Sky\Component\Confirmation\Enum\Status;
use Sky\Component\Confirmation\Enum\Type;
use Sky\Component\Confirmation\Messaging\Events\RequestedNotificationEvent;
use Sky\Component\Confirmation\Model\Builder\ConfirmationBuilder;
use Sky\Component\Core\Model\BuildableInterface;
use Sky\Component\Core\Model\Builder\BuilderInterface;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Model\Notification;
use Sky\Component\User\Model\User;

class Confirmation implements BuildableInterface, ContainsRecordedMessages
{
    use PrivateMessageRecorderCapabilities;

    private $id;
    private $user;
    private $code;
    private $type;
    private $status;
    private $createdAt;
    private $updatedAt;
    private $expireAt;
    private $notifications;

    /**
     * @param BuilderInterface|ConfirmationBuilder $builder
     */
    public function __construct(BuilderInterface $builder)
    {
        $this->id = $builder->id();
        $this->user = $builder->user();
        $this->code = $builder->code();
        $this->type = $builder->type();

        $this->status = Status::byValue(Status::PENDING);
        $this->createdAt = $this->updatedAt = new \DateTime();
        $this->expireAt = (clone $this->createdAt)->add(new \DateInterval(sprintf('PT%dS', $builder->ttl())));
        $this->notifications = new ArrayCollection();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function user(): User
    {
        return $this->user;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function type(): Type
    {
        return $this->type;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function createdAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function updatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function expireAt(): \DateTimeInterface
    {
        return $this->expireAt;
    }

    public function notifications(): Collection
    {
        null === $this->notifications && $this->notifications = new ArrayCollection();

        return $this->notifications;
    }

    public function assignNotification(Notification $notification): void
    {
        $this->notifications()->add($notification);
    }

    public function requestNotificationViaChannel(Channel $channel): void
    {
        $this->record(new RequestedNotificationEvent($this->id, $channel));
    }
}
