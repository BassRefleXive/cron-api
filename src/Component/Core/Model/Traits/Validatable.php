<?php

declare(strict_types=1);

namespace Sky\Component\Core\Model\Traits;

use Sky\Component\Core\Exception\ValidationException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait Validatable
{
    public function validate(ValidatorInterface $validator): void
    {
        if (count($violations = $validator->validate($this))) {
            throw new ValidationException($violations);
        }
    }
}
