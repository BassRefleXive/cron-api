<?php

declare(strict_types=1);

namespace Sky\Component\Core\Model;

use Symfony\Component\Validator\Validator\ValidatorInterface;

interface ValidatableInterface
{
    public function validate(ValidatorInterface $validator): void;
}
