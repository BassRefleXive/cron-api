<?php

declare(strict_types=1);

namespace Sky\Component\Core\Model\Builder;

use Sky\Component\Core\Model\BuildableInterface;

interface BuilderInterface
{
    public function build(): BuildableInterface;
}
