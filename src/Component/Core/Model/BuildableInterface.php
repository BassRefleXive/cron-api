<?php

declare(strict_types=1);

namespace Sky\Component\Core\Model;

use Sky\Component\Core\Model\Builder\BuilderInterface;

interface BuildableInterface
{
    public function __construct(BuilderInterface $builder);
}
