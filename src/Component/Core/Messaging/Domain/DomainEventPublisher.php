<?php

declare(strict_types=1);

namespace Sky\Component\Core\Messaging\Domain;

use LongRunning\Core\Cleaner;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\SymfonyBridge\Bus\EventBus;

class DomainEventPublisher implements Cleaner
{
    private $recorder;
    private $eventBus;

    public function __construct(ContainsRecordedMessages $recorder, EventBus $eventBus)
    {
        $this->recorder = $recorder;
        $this->eventBus = $eventBus;
    }

    public function publish()
    {
        $eventStream = $this->recorder->recordedMessages();

        if (!count($eventStream)) {
            return;
        }

        $this->recorder->eraseMessages();

        foreach ($eventStream as $event) {
            $this->eventBus->handle($event);
        }
    }

    public function discard()
    {
        $this->recorder->eraseMessages();
    }

    public function cleanUp()
    {
        $this->publish();
    }
}
