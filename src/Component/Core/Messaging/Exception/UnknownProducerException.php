<?php

declare(strict_types=1);

namespace Sky\Component\Core\Messaging\Exception;

final class UnknownProducerException extends \RuntimeException
{
    public static function serviceNotDefined(string $producer): self
    {
        return new self(
            sprintf(
                'Service for producer "%s" not found.',
                $producer
            )
        );
    }

    public static function undefinedProducer(string $method): self
    {
        return new self(
            sprintf(
                'No "producer" key defined in "%s"',
                $method
            )
        );
    }
}
