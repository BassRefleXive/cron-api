<?php

declare(strict_types=1);

namespace Sky\Component\Core\Messaging\Exception;

final class MessageConsumptionException extends \RuntimeException
{
    public static function create(string $message, int $code, \Throwable $previous): self
    {
        return new self($message, $code, $previous);
    }
}
