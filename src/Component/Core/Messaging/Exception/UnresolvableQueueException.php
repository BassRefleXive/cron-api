<?php

declare(strict_types=1);

namespace Sky\Component\Core\Messaging\Exception;

final class UnresolvableQueueException extends \RuntimeException
{
    public static function create(string $class): self
    {
        return new self(
            sprintf(
                'Queue for class "%s" can not be resolved.',
                $class
            )
        );
    }
}
