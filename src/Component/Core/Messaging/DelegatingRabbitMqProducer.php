<?php

declare(strict_types=1);

namespace Sky\Component\Core\Messaging;

use OldSound\RabbitMqBundle\RabbitMq\Fallback;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Sky\Component\Core\Messaging\Exception\UnknownProducerException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DelegatingRabbitMqProducer extends Fallback implements LoggerAwareInterface
{
    private $container;

    use LoggerAwareTrait;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function publish($msg, $routingKey = '', $props = []): void
    {
        if (!isset($props['producer'])) {
            throw UnknownProducerException::undefinedProducer(__METHOD__);
        }

        $this->getProducer($props['producer'])->publish($msg, $routingKey, $props);

        if ($this->logger instanceof LoggerInterface) {
            $this->logger->debug(sprintf('Message published to "%s" queue.', $props['producer']), [
                'body' => $msg,
                'routingKey' => $routingKey,
                'properties' => $props,
            ]);
        }
    }

    private function getProducer(string $name): ProducerInterface
    {
        $serviceId = sprintf('old_sound_rabbit_mq.%s_producer', $name);

        if (!$this->container->has($serviceId)) {
            throw UnknownProducerException::serviceNotDefined($name);
        }

        return $this->container->get($serviceId);
    }
}
