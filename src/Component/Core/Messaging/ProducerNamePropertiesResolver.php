<?php

declare(strict_types=1);

namespace Sky\Component\Core\Messaging;

use SimpleBus\Asynchronous\Properties\AdditionalPropertiesResolver;
use Sky\Component\Core\Messaging\Exception\UnresolvableQueueException;

class ProducerNamePropertiesResolver implements AdditionalPropertiesResolver
{
    private $commandsMap;
    private $eventsMap;

    public function __construct(array $commands, array $events)
    {
        $this->commandsMap = $commands;
        $this->eventsMap = $events;
    }

    public function resolveAdditionalPropertiesFor($message): array
    {
        $class = get_class($message);

        $queue = $this->commandsMap[$class] ?? $this->eventsMap[$class] ?? null;

        if (null === $queue) {
            throw UnresolvableQueueException::create($class);
        }

        return ['producer' => $queue];
    }
}
