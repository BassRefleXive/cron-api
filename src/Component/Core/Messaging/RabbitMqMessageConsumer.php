<?php

declare(strict_types=1);

namespace Sky\Component\Core\Messaging;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use SimpleBus\Asynchronous\Consumer\SerializedEnvelopeConsumer;
use SimpleBus\RabbitMQBundleBridge\Event\Events;
use SimpleBus\RabbitMQBundleBridge\Event\MessageConsumed;
use SimpleBus\RabbitMQBundleBridge\Event\MessageConsumptionFailed;
use Sky\Component\Core\Messaging\Exception\MessageConsumptionException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RabbitMqMessageConsumer implements ConsumerInterface
{
    private $consumer;
    private $dispatcher;

    public function __construct(SerializedEnvelopeConsumer $consumer, EventDispatcherInterface $dispatcher)
    {
        $this->consumer = $consumer;
        $this->dispatcher = $dispatcher;
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            $this->consumer->consume($msg->body);

            $this->dispatcher->dispatch(Events::MESSAGE_CONSUMED, new MessageConsumed($msg));

            $result = self::MSG_ACK;
        } catch (\Throwable $e) {
            if ($e instanceof \Error) {
                $e = MessageConsumptionException::create($e->getMessage(), $e->getCode(), $e);
            }

            $this->dispatcher->dispatch(Events::MESSAGE_CONSUMPTION_FAILED, new MessageConsumptionFailed($msg, $e));

            $result = self::MSG_REJECT_REQUEUE;
        }

        return $result;
    }
}
