<?php

declare(strict_types=1);

namespace Sky\Component\Core\CodeGenerator;

use Sky\Component\Core\CodeGenerator\SymbolProvider\SymbolProviderInterface;

class Generator implements GeneratorInterface
{
    private $symbolProvider;

    public function __construct(SymbolProviderInterface $symbolProvider)
    {
        $this->symbolProvider = $symbolProvider;
    }

    final public function generate(int $length): \Generator
    {
        $allowedSymbols = $this->symbolProvider->symbols();

        while (true) {
            $code = '';
            $max = strlen($allowedSymbols) - 1;

            for ($i = 0; $i < $length; ++$i) {
                $code .= $allowedSymbols[mt_rand(0, $max)];
            }

            yield $code;
        }
    }
}
