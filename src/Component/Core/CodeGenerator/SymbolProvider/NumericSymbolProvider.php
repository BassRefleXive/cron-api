<?php

declare(strict_types=1);

namespace Sky\Component\Core\CodeGenerator\SymbolProvider;

final class NumericSymbolProvider implements SymbolProviderInterface
{
    public function symbols(): string
    {
        return '0123456789';
    }
}
