<?php

declare(strict_types=1);

namespace Sky\Component\Core\CodeGenerator\SymbolProvider;

final class AlphaNumericSymbolProvider implements SymbolProviderInterface
{
    private $alphabeticSymbolProvider;
    private $numericSymbolProvider;

    public function __construct(SymbolProviderInterface $alphabeticSymbolProvider, SymbolProviderInterface $numericSymbolProvider)
    {
        $this->alphabeticSymbolProvider = $alphabeticSymbolProvider;
        $this->numericSymbolProvider = $numericSymbolProvider;
    }

    public function symbols(): string
    {
        return implode(
            '',
            [
                $this->alphabeticSymbolProvider->symbols(),
                $this->numericSymbolProvider->symbols(),
            ]
        );
    }
}
