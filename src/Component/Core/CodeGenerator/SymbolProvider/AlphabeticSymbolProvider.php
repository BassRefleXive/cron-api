<?php

declare(strict_types=1);

namespace Sky\Component\Core\CodeGenerator\SymbolProvider;

final class AlphabeticSymbolProvider implements SymbolProviderInterface
{
    public function symbols(): string
    {
        return 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
}
