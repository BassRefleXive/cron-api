<?php

declare(strict_types=1);

namespace Sky\Component\Core\CodeGenerator\SymbolProvider;

final class MixedSymbolProvider implements SymbolProviderInterface
{
    private $alphabeticSymbolProvider;
    private $numericSymbolProvider;
    private $symbolicSymbolProvider;

    public function __construct(
        SymbolProviderInterface $alphabeticSymbolProvider,
        SymbolProviderInterface $numericSymbolProvider,
        SymbolProviderInterface $symbolicSymbolProvider
    ) {
        $this->alphabeticSymbolProvider = $alphabeticSymbolProvider;
        $this->numericSymbolProvider = $numericSymbolProvider;
        $this->symbolicSymbolProvider = $symbolicSymbolProvider;
    }

    public function symbols(): string
    {
        return implode(
            '',
            [
                $this->alphabeticSymbolProvider->symbols(),
                $this->numericSymbolProvider->symbols(),
                $this->symbolicSymbolProvider->symbols(),
            ]
        );
    }
}
