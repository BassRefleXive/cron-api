<?php

declare(strict_types=1);

namespace Sky\Component\Core\CodeGenerator;

interface GeneratorInterface
{
    public function generate(int $length): \Generator;
}
