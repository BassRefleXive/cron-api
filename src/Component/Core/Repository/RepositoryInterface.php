<?php

declare(strict_types=1);

namespace Sky\Component\Core\Repository;

use Ramsey\Uuid\UuidInterface;

interface RepositoryInterface
{
    public function nextIdentity(): UuidInterface;

    public function save($entity): void;

    public function clear();

    public function remove(UuidInterface $id): void;

    public function getById(UuidInterface $id);

    public function all(): array;

    public function findByIds(array $ids): array;

    public function bulkSave(array $jobs): void;
}
