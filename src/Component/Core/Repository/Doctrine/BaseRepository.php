<?php

declare(strict_types=1);

namespace Sky\Component\Core\Repository\Doctrine;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Sky\Component\Core\Exception\MissingEntityException;
use Sky\Component\Core\Repository\RepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class BaseRepository extends ServiceEntityRepository implements RepositoryInterface
{
    final public function nextIdentity(): UuidInterface
    {
        return Uuid::uuid4();
    }

    public function save($entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }

    public function remove(UuidInterface $id): void
    {
        $entity = $this->_em->getReference($this->_entityName, $id);

        $this->_em->remove($entity);
        $this->_em->flush($entity);
    }

    public function getById(UuidInterface $id)
    {
        $entity = $this->find($id);

        if (null === $entity) {
            throw MissingEntityException::missingById($id, $this->_entityName);
        }

        return $entity;
    }

    public function all(): array
    {
        return $this->findAll();
    }

    public function findByIds(array $ids): array
    {
        return $this->findBy(['id' => $ids]);
    }

    public function bulkSave(array $jobs): void
    {
        foreach ($jobs as $job) {
            $this->_em->persist($job);
        }

        $this->_em->flush();
    }
}
