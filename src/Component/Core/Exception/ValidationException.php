<?php

declare(strict_types=1);

namespace Sky\Component\Core\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

final class ValidationException extends \RuntimeException
{
    private $violations;

    public function __construct(ConstraintViolationListInterface $violations)
    {
        parent::__construct();

        $this->violations = $violations;
    }

    public function violations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
