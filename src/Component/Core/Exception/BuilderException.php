<?php

declare(strict_types=1);

namespace Sky\Component\Core\Exception;

final class BuilderException extends \InvalidArgumentException
{
    public static function missingPropertyValue(string $property, string $target): self
    {
        return new self(sprintf('Property "%s" should be set when building %s.', $property, $target));
    }
}
