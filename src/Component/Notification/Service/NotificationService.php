<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Service;

use Sky\Component\Confirmation\Model\Confirmation;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Factory\Message\MessageFactoryResolverMap;
use Sky\Component\Notification\Model\Builder\NotificationBuilder;
use Sky\Component\Notification\Model\Notification;
use Sky\Component\Notification\Repository\NotificationRepositoryInterface;

class NotificationService
{
    private $notificationRepository;
    private $messageFactoryResolverMap;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        MessageFactoryResolverMap $messageFactoryResolverMap
    ) {
        $this->notificationRepository = $notificationRepository;
        $this->messageFactoryResolverMap = $messageFactoryResolverMap;
    }

    public function notifyConfirmation(Confirmation $confirmation, Channel $channel): Notification
    {
        $notification = (new NotificationBuilder())
            ->withId($this->notificationRepository->nextIdentity())
            ->withChannel($channel)
            ->withText($this->messageFactoryResolverMap->get($channel)->resolve($confirmation)->create($confirmation))
            ->build();

        $this->notificationRepository->save($notification);

        return $notification;
    }
}
