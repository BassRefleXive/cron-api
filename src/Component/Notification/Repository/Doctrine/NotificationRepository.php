<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Repository\Doctrine;

use Doctrine\Common\Persistence\ManagerRegistry;
use Sky\Component\Core\Repository\Doctrine\BaseRepository;
use Sky\Component\Notification\Model\Notification;
use Sky\Component\Notification\Repository\NotificationRepositoryInterface;

class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }
}
