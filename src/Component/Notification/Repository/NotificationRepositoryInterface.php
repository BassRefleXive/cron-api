<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Repository;

use Sky\Component\Core\Repository\RepositoryInterface;

interface NotificationRepositoryInterface extends RepositoryInterface
{
}
