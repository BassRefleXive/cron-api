<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Messaging\Command;

use Ramsey\Uuid\UuidInterface;

class SendNotificationCommand
{
    private $notificationId;

    public function __construct(UuidInterface $notificationId)
    {
        $this->notificationId = $notificationId;
    }

    public function notificationId(): UuidInterface
    {
        return $this->notificationId;
    }
}
