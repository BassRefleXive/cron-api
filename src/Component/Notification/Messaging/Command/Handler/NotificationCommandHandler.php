<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Messaging\Command\Handler;

use Sky\Component\Notification\Messaging\Command\SendNotificationCommand;
use Sky\Component\Notification\Model\Notification;
use Sky\Component\Notification\Repository\NotificationRepositoryInterface;

class NotificationCommandHandler
{
    private $notificationRepository;

    public function __construct(NotificationRepositoryInterface $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function onSendNotificationCommand(SendNotificationCommand $command): void
    {
        /** @var Notification $notification */
        $notification = $this->notificationRepository->getById($command->notificationId());

        echo sprintf(
                'Send notification "%s". Text: "%s".',
                $command->notificationId(),
                $notification->text()
            ).PHP_EOL;
    }
}
