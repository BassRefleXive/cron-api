<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Messaging\Event\Listener;

use SimpleBus\SymfonyBridge\Bus\CommandBus;
use Sky\Component\Confirmation\Messaging\Events\RequestedNotificationEvent;
use Sky\Component\Confirmation\Service\ConfirmationService;
use Sky\Component\Notification\Factory\Notification\NotificationFactoryResolver;
use Sky\Component\Notification\Repository\NotificationRepositoryInterface;

class ConfirmationEventsListener extends AbstractNotificationListener
{
    private $confirmationService;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        NotificationFactoryResolver $notificationFactoryResolver,
        CommandBus $commandBus,
        ConfirmationService $confirmationService
    ) {
        parent::__construct($notificationRepository, $notificationFactoryResolver, $commandBus);

        $this->confirmationService = $confirmationService;
    }

    public function onRequestedConfirmationNotification(RequestedNotificationEvent $event): void
    {
        $confirmation = $this->confirmationService->getById($event->id());

        $notification = $this->create($event->channel(), $confirmation);

        $this->confirmationService->assignConfirmationNotification($confirmation, $notification);

        $this->recordSendCommand($notification);
    }
}
