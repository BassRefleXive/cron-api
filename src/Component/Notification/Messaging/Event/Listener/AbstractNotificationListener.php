<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Messaging\Event\Listener;

use SimpleBus\SymfonyBridge\Bus\CommandBus;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Factory\Notification\NotificationFactoryResolver;
use Sky\Component\Notification\Messaging\Command\SendNotificationCommand;
use Sky\Component\Notification\Model\Notification;
use Sky\Component\Notification\Repository\NotificationRepositoryInterface;

abstract class AbstractNotificationListener
{
    private $notificationRepository;
    private $notificationFactoryResolver;
    private $commandBus;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        NotificationFactoryResolver $notificationFactoryResolver,
        CommandBus $commandBus
    ) {
        $this->notificationRepository = $notificationRepository;
        $this->notificationFactoryResolver = $notificationFactoryResolver;
        $this->commandBus = $commandBus;
    }

    final protected function create(Channel $channel, $object): Notification
    {
        $notification = $this->notificationFactoryResolver->resolve($object)->create($channel, $object);

        $this->notificationRepository->save($notification);

        return $notification;
    }

    final protected function recordSendCommand(Notification $notification): void
    {
        $this->commandBus->handle(
            new SendNotificationCommand($notification->id())
        );
    }
}
