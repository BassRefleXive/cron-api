<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Enum;

use MabeEnum\Enum;

final class Channel extends Enum
{
    public const EMAIL = 0;
    public const SMS = 1;
    public const SLACK = 2;
}
