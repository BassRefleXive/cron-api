<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Enum;

use MabeEnum\Enum;

final class Status extends Enum
{
    public const PENDING = 0;
    public const SENT = 1;
    public const DELIVERED = 2;
}
