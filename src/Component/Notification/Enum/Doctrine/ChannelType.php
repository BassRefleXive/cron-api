<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Enum\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Sky\Component\Notification\Enum\Channel;

class ChannelType extends Type
{
    private const NAME = 'notification_channel';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getSmallIntTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Channel
    {
        if (null !== $value) {
            return Channel::byValue((int) $value);
        }

        return null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        /* @var Channel $value */
        return null !== $value
            ? $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
