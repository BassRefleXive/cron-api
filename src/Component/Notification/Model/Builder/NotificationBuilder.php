<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Model\Builder;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\Core\Exception\BuilderException;
use Sky\Component\Core\Model\BuildableInterface;
use Sky\Component\Core\Model\Builder\BuilderInterface;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Model\Notification;

class NotificationBuilder implements BuilderInterface
{
    private $id;
    private $text;
    private $channel;

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function withId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function text(): string
    {
        return $this->text;
    }

    public function withText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function channel(): Channel
    {
        return $this->channel;
    }

    public function withChannel(Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return BuildableInterface|Notification
     */
    public function build(): BuildableInterface
    {
        $this->validate();

        return new Notification($this);
    }

    private function validate(): void
    {
        foreach ($this as $property => $value) {
            if (null === $value) {
                throw BuilderException::missingPropertyValue($property, Notification::class);
            }
        }
    }
}
