<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Model;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\Core\Model\BuildableInterface;
use Sky\Component\Core\Model\Builder\BuilderInterface;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Enum\Status;
use Sky\Component\Notification\Model\Builder\NotificationBuilder;

class Notification implements BuildableInterface
{
    private $id;
    private $text;
    private $channel;
    private $status;
    private $createdAt;
    private $sentAt;
    private $deliveredAt;

    /**
     * @param BuilderInterface|NotificationBuilder $builder
     */
    public function __construct(BuilderInterface $builder)
    {
        $this->id = $builder->id();
        $this->text = $builder->text();
        $this->channel = $builder->channel();

        $this->status = Status::byValue(Status::PENDING);
        $this->createdAt = new \DateTime();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function text(): string
    {
        return $this->text;
    }

    public function channel(): Channel
    {
        return $this->channel;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function createdAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function sentAt(): ?\DateTimeInterface
    {
        return $this->sentAt;
    }

    public function deliveredAt(): ?\DateTimeInterface
    {
        return $this->deliveredAt;
    }
}
