<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Exception;

class MessageFactoryException extends \LogicException
{
    public static function alreadyDefined(string $class): self
    {
        return new self(
            sprintf('Message factory for subject "%s" already defined.', $class)
        );
    }

    public static function missing(string $class): self
    {
        return new self(
            sprintf('Message factory for subject "%s" missing.', $class)
        );
    }
}
