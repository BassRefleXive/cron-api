<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Exception;

class NotificationFactoryException extends \LogicException
{
    public static function alreadyDefined(string $class): self
    {
        return new self(
            sprintf('Notification factory for subject "%s" already defined.', $class)
        );
    }

    public static function missing(string $class): self
    {
        return new self(
            sprintf('Notification factory for subject "%s" missing.', $class)
        );
    }
}
