<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Exception;

use Sky\Component\Notification\Enum\Channel;

class MessageFactoryResolverException extends \LogicException
{
    public static function alreadyDefined(Channel $channel): self
    {
        return new self(
            sprintf('Factory for channel "%s" already defined.', $channel->getValue())
        );
    }

    public static function missing(Channel $channel): self
    {
        return new self(
            sprintf('Factory for channel "%s" missing.', $channel->getValue())
        );
    }

    public static function unresolvableFactory(Channel $channel): self
    {
        return new self(
            sprintf('Cannot resolve channel factory for channel "%s".', $channel->getValue())
        );
    }
}
