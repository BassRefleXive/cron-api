<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Notification;

class NotificationFactoryResolver
{
    private $factoryMap;

    public function __construct(FactoryMap $factoryMap)
    {
        $this->factoryMap = $factoryMap;
    }

    public function resolve($subject): NotificationFactoryInterface
    {
        $class = get_class($subject);

        return $this->factoryMap->get($class);
    }
}
