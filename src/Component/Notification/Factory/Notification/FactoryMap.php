<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Notification;

use Sky\Component\Notification\Exception\NotificationFactoryException;

class FactoryMap
{
    private $factories;

    public function add(string $subjectClass, NotificationFactoryInterface $factory): void
    {
        if (isset($this->factories[$subjectClass])) {
            throw NotificationFactoryException::alreadyDefined($subjectClass);
        }

        $this->factories[$subjectClass] = $factory;
    }

    public function get(string $subjectClass): NotificationFactoryInterface
    {
        if (!isset($this->factories[$subjectClass])) {
            throw NotificationFactoryException::missing($subjectClass);
        }

        return $this->factories[$subjectClass];
    }
}
