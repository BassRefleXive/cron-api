<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Notification;

use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Model\Notification;

interface NotificationFactoryInterface
{
    public function create(Channel $channel, $object): Notification;
}
