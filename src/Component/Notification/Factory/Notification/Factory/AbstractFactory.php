<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Notification\Factory;

use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Factory\Message\MessageFactoryResolverMap;
use Sky\Component\Notification\Factory\Notification\NotificationFactoryInterface;
use Sky\Component\Notification\Repository\NotificationRepositoryInterface;

abstract class AbstractFactory implements NotificationFactoryInterface
{
    protected $notificationRepository;
    private $messageFactoryResolverMap;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        MessageFactoryResolverMap $messageFactoryResolverMap
    ) {
        $this->notificationRepository = $notificationRepository;
        $this->messageFactoryResolverMap = $messageFactoryResolverMap;
    }

    final protected function createText(Channel $channel, $object): string
    {
        return $this->messageFactoryResolverMap->get($channel)->resolve($object)->create($object);
    }
}
