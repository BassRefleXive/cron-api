<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Notification\Factory;

use Sky\Component\Confirmation\Model\Confirmation;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Model\Builder\NotificationBuilder;
use Sky\Component\Notification\Model\Notification;

class ConfirmationNotificationFactory extends AbstractFactory
{
    /**
     * @param Channel      $channel
     * @param Confirmation $object
     *
     * @return Notification
     */
    public function create(Channel $channel, $object): Notification
    {
        $notification = (new NotificationBuilder())
            ->withId($this->notificationRepository->nextIdentity())
            ->withChannel($channel)
            ->withText($this->createText($channel, $object))
            ->build();

        return $notification;
    }
}
