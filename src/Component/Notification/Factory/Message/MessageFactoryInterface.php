<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Message;

interface MessageFactoryInterface
{
    public function create($subject): string;
}
