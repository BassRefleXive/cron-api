<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Message;

use Sky\Component\Notification\Exception\MessageFactoryException;

class FactoryMap
{
    private $factories;

    public function add(string $subjectClass, MessageFactoryInterface $factory): void
    {
        if (isset($this->factories[$subjectClass])) {
            throw MessageFactoryException::alreadyDefined($subjectClass);
        }

        $this->factories[$subjectClass] = $factory;
    }

    public function get(string $subjectClass): MessageFactoryInterface
    {
        if (!isset($this->factories[$subjectClass])) {
            throw MessageFactoryException::missing($subjectClass);
        }

        return $this->factories[$subjectClass];
    }
}
