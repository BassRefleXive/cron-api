<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Message\Sms;

use Sky\Component\Confirmation\Model\Confirmation;
use Sky\Component\Notification\Factory\Message\MessageFactoryInterface;

class ConfirmationMessageFactory implements MessageFactoryInterface
{
    /**
     * @param Confirmation $subject
     *
     * @return string
     */
    public function create($subject): string
    {
        return 'SMS::ConfirmationMessageFactory::create';
    }
}
