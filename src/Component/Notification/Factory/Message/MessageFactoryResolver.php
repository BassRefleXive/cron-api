<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Message;

class MessageFactoryResolver
{
    private $factoryMap;

    public function __construct(FactoryMap $factoryMap)
    {
        $this->factoryMap = $factoryMap;
    }

    public function resolve($subject): MessageFactoryInterface
    {
        $class = get_class($subject);

        return $this->factoryMap->get($class);
    }
}
