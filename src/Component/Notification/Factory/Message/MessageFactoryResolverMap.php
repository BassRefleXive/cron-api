<?php

declare(strict_types=1);

namespace Sky\Component\Notification\Factory\Message;

use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Exception\MessageFactoryResolverException;

class MessageFactoryResolverMap
{
    private $resolvers;

    public function add(int $channel, MessageFactoryResolver $resolver): void
    {
        $channel = Channel::byValue($channel);

        if (isset($this->resolvers[$channel->getValue()])) {
            throw MessageFactoryResolverException::alreadyDefined($channel);
        }

        $this->resolvers[$channel->getValue()] = $resolver;
    }

    public function get(Channel $channel): MessageFactoryResolver
    {
        if (!isset($this->resolvers[$channel->getValue()])) {
            throw MessageFactoryResolverException::missing($channel);
        }

        return $this->resolvers[$channel->getValue()];
    }
}
