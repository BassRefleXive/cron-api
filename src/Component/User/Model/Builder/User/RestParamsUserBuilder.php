<?php

declare(strict_types=1);

namespace Sky\Component\User\Model\Builder\User;

use Sky\Component\Core\Model\BuildableInterface;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sky\Component\User\Enum\RoleSet;
use Sky\Component\User\Model\User;

class RestParamsUserBuilder extends UserBuilder
{
    private $params;

    public function __construct(ParamFetcherInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @return User
     */
    public function build(): BuildableInterface
    {
        $this->title = $this->params->get('title');
        $this->email = $this->params->get('email');
        $this->password = $this->params->get('password');
        $this->acl = new RoleSet();

        return parent::build();
    }
}
