<?php

declare(strict_types=1);

namespace Sky\Component\User\Model\Builder\User;

use Ramsey\Uuid\Uuid;
use Sky\Component\Core\Model\BuildableInterface;
use Sky\Component\User\Enum\Role;
use Sky\Component\User\Enum\RoleSet;
use Sky\Component\User\Model\User;
use Symfony\Component\Console\Input\InputInterface;

class ConsoleUserBuilder extends UserBuilder
{
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    /**
     * @return User
     */
    public function build(): BuildableInterface
    {
        $this->id = Uuid::fromString($this->input->getOption('id'));
        $this->title = $this->input->getOption('title');
        $this->email = $this->input->getOption('email');
        $this->password = $this->input->getOption('password');
        $this->apiKey = $this->input->getOption('api-key');

        if (null !== $parentId = $this->input->getOption('parent-id')) {
            $this->parentId = Uuid::fromString($parentId);
        }

        $this->acl = new RoleSet();

        if (null !== $roles = $this->input->getOption('roles')) {
            $roles = explode(',', $roles);

            foreach ($roles as $role) {
                $this->acl->attach(Role::byValue((int) $role));
            }
        }

        return parent::build();
    }
}
