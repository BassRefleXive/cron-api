<?php

declare(strict_types=1);

namespace Sky\Component\User\Model\Builder\User;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\Core\Exception\BuilderException;
use Sky\Component\Core\Model\BuildableInterface;
use Sky\Component\Core\Model\Builder\BuilderInterface;
use Sky\Component\User\Enum\RoleSet;
use Sky\Component\User\Model\User;

class UserBuilder implements BuilderInterface
{
    protected $id;
    protected $parentId;
    protected $title;
    protected $email;
    protected $password;
    protected $acl;
    protected $apiKey;

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function withId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function parentId(): ?UuidInterface
    {
        return $this->parentId;
    }

    public function withParentId(UuidInterface $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function acl(): RoleSet
    {
        return $this->acl;
    }

    public function apiKey(): string
    {
        return $this->apiKey;
    }

    public function withApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * @return BuildableInterface|User
     */
    public function build(): BuildableInterface
    {
        $this->validate();

        return new User($this);
    }

    private function validate(): void
    {
        $excluded = ['parentId', 'apiKey'];

        foreach ($this as $property => $value) {
            if (in_array($property, $excluded, true)) {
                continue;
            }

            if (null === $value) {
                throw BuilderException::missingPropertyValue($property, User::class);
            }
        }
    }
}
