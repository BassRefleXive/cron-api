<?php

declare(strict_types=1);

namespace Sky\Component\User\Model;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\Core\Model\BuildableInterface;
use Sky\Component\Core\Model\Builder\BuilderInterface;
use Sky\Component\Core\Model\Traits\Validatable;
use Sky\Component\Core\Model\ValidatableInterface;
use Sky\Component\User\Enum\Role;
use Sky\Component\User\Enum\RoleSet;
use Sky\Component\User\Exception\RoleManagementException;
use Sky\Component\User\Model\Builder\User\UserBuilder;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements BuildableInterface, ValidatableInterface, UserInterface
{
    use Validatable;

    private $id;
    private $parentId;
    private $title;
    private $email;
    private $password;
    private $acl;
    private $apiKey;

    /**
     * @param UserBuilder $builder
     */
    public function __construct(BuilderInterface $builder)
    {
        $this->id = $builder->id();
        $this->parentId = $builder->parentId();
        $this->title = $builder->title();
        $this->email = $builder->email();
        $this->password = $builder->password();
        $this->acl = $builder->acl();
        $this->apiKey = $builder->apiKey();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function parentId(): ?UuidInterface
    {
        return $this->parentId;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function changePassword(string $password): void
    {
        $this->password = $password;
    }

    public function roles(): RoleSet
    {
        return $this->acl;
    }

    public function apiKey(): string
    {
        return $this->apiKey;
    }

    public function changeApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    public function isMaster(): bool
    {
        return null === $this->parentId;
    }

    public function isSlave(): bool
    {
        return !$this->isMaster();
    }

    public function addRole(Role $role): void
    {
        if (!$this->isSlave()) {
            throw RoleManagementException::availableForSlaveOnly();
        }

        $this->roles()->attach($role);
    }

    public function removeRole(Role $role): void
    {
        if (!$this->isSlave()) {
            throw RoleManagementException::availableForSlaveOnly();
        }

        $this->roles()->detach($role);
    }

    /**
     * Methods required by UserInterface.
     */
    public function getRoles()
    {
        return $this->acl->getNames();
    }

    public function getPassword()
    {
        return $this->password();
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
    }
}
