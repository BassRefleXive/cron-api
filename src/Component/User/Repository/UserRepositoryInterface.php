<?php

declare(strict_types=1);

namespace Sky\Component\User\Repository;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\Core\Repository\RepositoryInterface;
use Sky\Component\User\Model\User;

interface UserRepositoryInterface extends RepositoryInterface
{
    public function getByApiKey(string $apiKey): User;

    public function getByEmail(string $email): User;

    public function isOwnedByUser(UuidInterface $master, UuidInterface $slave): bool;
}
