<?php

declare(strict_types=1);

namespace Sky\Component\User\Repository\Doctrine;

use Doctrine\Common\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;
use Sky\Component\Core\Repository\Doctrine\BaseRepository;
use Sky\Component\User\Exception\UserNotFoundException;
use Sky\Component\User\Model\User;
use Sky\Component\User\Repository\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getByApiKey(string $apiKey): User
    {
        /** @var User|null $user */
        $user = $this->findOneBy(['apiKey' => $apiKey]);

        if (null === $user) {
            throw UserNotFoundException::missingByApiKey($apiKey);
        }

        return $user;
    }

    public function getByEmail(string $email): User
    {
        /** @var User|null $user */
        $user = $this->findOneBy(['email' => $email]);

        if (null === $user) {
            throw UserNotFoundException::missingByEmail($email);
        }

        return $user;
    }

    public function isOwnedByUser(UuidInterface $master, UuidInterface $slave): bool
    {
        $qb = $this->createQueryBuilder('u');

        $result = $qb->select('u')
            ->where('u.id = :slave_id')
            ->andWhere('u.parentId = :owner_id')
            ->setParameter('slave_id', $slave)
            ->setParameter('owner_id', $master)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return null !== $result;
    }
}
