<?php

declare(strict_types=1);

namespace Sky\Component\User\Service;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\User\Exception\UserNotBelongsToMasterException;
use Sky\Component\User\Model\User;
use Sky\Component\User\Repository\UserRepositoryInterface;

class UserService
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getOwned(User $owner, UuidInterface $id): User
    {
        if ($owner->id()->equals($id)) {
            return $owner;
        }

        if (!$this->userRepository->isOwnedByUser($owner->id(), $id)) {
            throw UserNotBelongsToMasterException::create($owner->id(), $id);
        }

        return $this->userRepository->getById($id);
    }
}
