<?php

declare(strict_types=1);

namespace Sky\Component\User\Service;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\User\Enum\Role;
use Sky\Component\User\Model\Builder\User\UserBuilder;
use Sky\Component\User\Model\User;
use Sky\Component\User\Repository\UserRepositoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationService
{
    private $userRepository;
    private $passwordEncoder;
    private $apiKeyGenerator;
    private $validator;

    public function __construct(
        UserRepositoryInterface $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        ApiKeyGenerator $apiKeyGenerator,
        ValidatorInterface $validator
    ) {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->apiKeyGenerator = $apiKeyGenerator;
        $this->validator = $validator;
    }

    public function createMaster(UserBuilder $builder): User
    {
        $user = $builder
            ->withId($this->userRepository->nextIdentity())
            ->withApiKey($this->apiKeyGenerator->generate())
            ->build();

        $user->validate($this->validator);

        $user->changePassword($this->passwordEncoder->encodePassword($user, $user->password()));

        foreach (Role::getEnumerators() as $role) {
            $user->roles()->attach($role);
        }

        $this->userRepository->save($user);

        return $user;
    }

    public function createSlave(UuidInterface $masterId, UserBuilder $builder): User
    {
        $user = $builder
            ->withId($this->userRepository->nextIdentity())
            ->withApiKey($this->apiKeyGenerator->generate())
            ->withParentId($masterId)
            ->build();

        $user->validate($this->validator);

        $user->changePassword($this->passwordEncoder->encodePassword($user, $user->password()));

        $this->userRepository->save($user);

        return $user;
    }
}
