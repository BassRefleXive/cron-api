<?php

declare(strict_types=1);

namespace Sky\Component\User\Service;

use Sky\Component\Confirmation\Service\ConfirmationService;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\Notification\Service\NotificationService;
use Sky\Component\User\Repository\UserRepositoryInterface;

class PasswordService
{
    private $userRepository;
    private $confirmationService;
    private $notificationService;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ConfirmationService $confirmationService,
        NotificationService $notificationService
    ) {
        $this->userRepository = $userRepository;
        $this->confirmationService = $confirmationService;
        $this->notificationService = $notificationService;
    }

    public function restorePasswordRequest(string $email, Channel $channel): void
    {
        $user = $this->userRepository->getByEmail($email);

        $this->confirmationService->createResetPasswordConfirmation($user, $channel);
    }
}
