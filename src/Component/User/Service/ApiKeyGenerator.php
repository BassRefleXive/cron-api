<?php

declare(strict_types=1);

namespace Sky\Component\User\Service;

use Sky\Component\Core\CodeGenerator\GeneratorInterface;
use Sky\Component\User\Exception\UserNotFoundException;
use Sky\Component\User\Repository\UserRepositoryInterface;

class ApiKeyGenerator
{
    public const LENGTH = 32;

    private $userRepository;
    private $codeGenerator;

    public function __construct(UserRepositoryInterface $userRepository, GeneratorInterface $codeGenerator)
    {
        $this->userRepository = $userRepository;
        $this->codeGenerator = $codeGenerator;
    }

    public function generate(): string
    {
        foreach ($this->codeGenerator->generate(self::LENGTH) as $apiKey) {
            try {
                $this->userRepository->getByApiKey($apiKey);
            } catch (UserNotFoundException $e) {
                return $apiKey;
            }
        }
    }
}
