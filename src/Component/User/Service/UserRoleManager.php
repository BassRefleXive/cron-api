<?php

declare(strict_types=1);

namespace Sky\Component\User\Service;

use Ramsey\Uuid\UuidInterface;
use Sky\Component\User\Enum\Role;
use Sky\Component\User\Exception\UserNotBelongsToMasterException;
use Sky\Component\User\Model\User;
use Sky\Component\User\Repository\UserRepositoryInterface;

class UserRoleManager
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function add(User $owner, UuidInterface $userId, Role $role): User
    {
        if (!$this->userRepository->isOwnedByUser($owner->id(), $userId)) {
            throw UserNotBelongsToMasterException::create($owner->id(), $userId);
        }

        /** @var User $user */
        $user = $this->userRepository->getById($userId);

        $user->addRole($role);

        $this->userRepository->save($user);

        return $user;
    }

    public function remove(User $owner, UuidInterface $userId, Role $role): User
    {
        if (!$this->userRepository->isOwnedByUser($owner->id(), $userId)) {
            throw UserNotBelongsToMasterException::create($owner->id(), $userId);
        }

        /** @var User $user */
        $user = $this->userRepository->getById($userId);

        $user->removeRole($role);

        $this->userRepository->save($user);

        return $user;
    }
}
