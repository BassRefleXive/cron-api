<?php

declare(strict_types=1);

namespace Sky\Component\User\Exception;

final class UserAlreadyExistsException extends \LogicException
{
    public static function existsWithEmail(string $email): self
    {
        return new self(
            sprintf(
                'User with email "%s" already exists.',
                $email
            )
        );
    }
}
