<?php

declare(strict_types=1);

namespace Sky\Component\User\Exception;

use Ramsey\Uuid\UuidInterface;

final class UserNotBelongsToMasterException extends \LogicException
{
    public static function create(UuidInterface $master, UuidInterface $slave): self
    {
        return new self(
            sprintf(
                'User with id "%s" does not belongs to user "%s".',
                $slave,
                $master
            )
        );
    }
}
