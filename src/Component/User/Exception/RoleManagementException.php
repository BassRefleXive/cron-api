<?php

declare(strict_types=1);

namespace Sky\Component\User\Exception;

final class RoleManagementException extends \LogicException
{
    public static function availableForSlaveOnly(): self
    {
        return new self('Role management available for slave users only.');
    }
}
