<?php

declare(strict_types=1);

namespace Sky\Component\User\Exception;

final class UserNotFoundException extends \RuntimeException
{
    public static function missingByUsername(string $username): self
    {
        return new self(
            sprintf(
                'User "%s" not found.',
                $username
            )
        );
    }

    public static function missingByApiKey(string $apiKey): self
    {
        return new self(
            sprintf(
                'User with api key "%s" not found.',
                $apiKey
            )
        );
    }

    public static function missingByEmail(string $email): self
    {
        return new self(
            sprintf(
                'User with email "%s" not found.',
                $email
            )
        );
    }
}
