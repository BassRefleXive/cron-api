<?php

declare(strict_types=1);

namespace Sky\Component\User\Enum;

use MabeEnum\Enum;

final class Role extends Enum
{
    public const ROLE_MASTER = 0;

    public const ROLE_MONITOR_CREATE = 1;
    public const ROLE_MONITOR_EDIT = 2;
    public const ROLE_MONITOR_REMOVE = 3;
}
