<?php

declare(strict_types=1);

namespace Sky\Component\User\Enum;

use MabeEnum\EnumSet;

final class RoleSet extends EnumSet
{
    public function __construct()
    {
        parent::__construct(Role::class);
    }
}
