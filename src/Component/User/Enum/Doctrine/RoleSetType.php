<?php

declare(strict_types=1);

namespace Sky\Component\User\Enum\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Sky\Component\User\Enum\RoleSet;

class RoleSetType extends Type
{
    private const NAME = 'user_role_set';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getBinaryTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): RoleSet
    {
        $roles = new RoleSet();

        if (null !== $value) {
            $roles->setBinaryBitsetLe($value);
        }

        return $roles;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /* @var RoleSet $value */
        return null !== $value
            ? $value->getBinaryBitsetLe()
            : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
