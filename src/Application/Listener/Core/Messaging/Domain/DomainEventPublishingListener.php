<?php

declare(strict_types=1);

namespace Sky\Application\Listener\Core\Messaging\Domain;

use Sky\Component\Core\Messaging\Domain\DomainEventPublisher;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class DomainEventPublishingListener implements EventSubscriberInterface
{
    private $publisher;

    public function __construct(DomainEventPublisher $publisher)
    {
        $this->publisher = $publisher;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => ['onKernelResponse', 1024],
            KernelEvents::EXCEPTION => ['onKernelException', 1024],
            ConsoleEvents::TERMINATE => ['onConsoleTerminate', 1024],
            ConsoleEvents::ERROR => ['onConsoleError', 1024],
        ];
    }

    public function onKernelResponse(FilterResponseEvent $event): void
    {
        $this->publisher->publish();
    }

    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $this->publisher->discard();
    }

    public function onConsoleTerminate(ConsoleTerminateEvent $event): void
    {
        $this->publisher->publish();
    }

    public function onConsoleError(ConsoleErrorEvent $event): void
    {
        $this->publisher->discard();
    }
}
