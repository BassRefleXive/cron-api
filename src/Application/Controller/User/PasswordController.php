<?php

declare(strict_types=1);

namespace Sky\Application\Controller\User;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sky\Component\Notification\Enum\Channel;
use Sky\Component\User\Service\PasswordService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @FOSRest\Route("/user/password")
 */
class PasswordController
{
    private $passwordService;

    public function __construct(PasswordService $passwordService)
    {
        $this->passwordService = $passwordService;
    }

    /**
     * @FOSRest\Post("/restore"))
     *
     * @FOSRest\RequestParam(name="email", description="Email.")
     * @FOSRest\RequestParam(name="channel", description="Channel to send verification code to.")
     */
    public function restoreRequest(ParamFetcherInterface $params)
    {
        $this->passwordService->restorePasswordRequest(
            $params->get('email'),
            Channel::byName($params->get('channel'))
        );

        return new Response('', Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Post("/restore/confirm"))
     *
     * @FOSRest\RequestParam(name="code", description="Verification code.")
     * @FOSRest\RequestParam(name="password", description="New password.")
     */
    public function restoreConfirm(ParamFetcherInterface $params)
    {
    }

    /**
     * @FOSRest\Post("/change"))
     *
     * @FOSRest\RequestParam(name="channel", description="Channel to send verification code to.")
     */
    public function changeRequest(UserInterface $user)
    {
    }

    /**
     * @FOSRest\Post("/change/confirm"))
     *
     * @FOSRest\RequestParam(name="code", description="Verification code.")
     * @FOSRest\RequestParam(name="password", description="New password.")
     */
    public function changeConfirm(UserInterface $user)
    {
    }
}
