<?php

declare(strict_types=1);

namespace Sky\Application\Controller\User;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Ramsey\Uuid\Uuid;
use Sky\Component\User\Enum\Role;
use Sky\Component\User\Model\User;
use Sky\Component\User\Service\UserRoleManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @FOSRest\Route("/user")
 */
class RoleController
{
    private $userRoleManager;

    public function __construct(UserRoleManager $userRoleManager)
    {
        $this->userRoleManager = $userRoleManager;
    }

    /**
     * @FOSRest\Post("/{id}/role"))
     *
     * @FOSRest\RequestParam(name="role", description="Role to add to user")
     */
    public function addRole(UserInterface $user, string $id, ParamFetcherInterface $params): View
    {
        /** @var User $user */
        $user = $this->userRoleManager->add($user, Uuid::fromString($id), Role::byName($params->get('role')));

        return View::create($user, Response::HTTP_CREATED)->setContext(
            (new Context())
                ->addGroup('id')
                ->addGroup('user')
        );
    }

    /**
     * @FOSRest\Delete("/{id}/role/{role}"))
     */
    public function removeRole(UserInterface $user, string $id, string $role): View
    {
        /** @var User $user */
        $user = $this->userRoleManager->remove($user, Uuid::fromString($id), Role::byName($role));

        return View::create($user, Response::HTTP_OK)->setContext(
            (new Context())
                ->addGroup('id')
                ->addGroup('user')
        );
    }
}
