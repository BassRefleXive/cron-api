<?php

declare(strict_types=1);

namespace Sky\Application\Controller\User;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;
use Ramsey\Uuid\Uuid;
use Sky\Component\User\Model\User;
use Sky\Component\User\Service\UserService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @FOSRest\Route("/user")
 */
class UserController
{
    /**
     * @FOSRest\Get("/me"))
     */
    public function me(UserInterface $user): View
    {
        /* @var User $user */
        return View::create($user, Response::HTTP_OK)->setContext(
            (new Context())
                ->addGroup('id')
                ->addGroup('user')
        );
    }

    /**
     * @FOSRest\Get("/{id}"))
     */
    public function get(UserService $service, UserInterface $user, string $id): View
    {
        /** @var User $user */
        $user = $service->getOwned($user, Uuid::fromString($id));

        return View::create($user, Response::HTTP_OK)->setContext(
            (new Context())
                ->addGroup('id')
                ->addGroup('user')
        );
    }
}
