<?php

declare(strict_types=1);

namespace Sky\Application\Controller\User;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Sky\Component\User\Model\Builder\User\RestParamsUserBuilder;
use Sky\Component\User\Model\User;
use Sky\Component\User\Service\RegistrationService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @FOSRest\Route("/user/registration")
 */
class RegistrationController
{
    private $service;

    public function __construct(RegistrationService $service)
    {
        $this->service = $service;
    }

    /**
     * @FOSRest\Post("/")
     *
     * @FOSRest\RequestParam(name="title", description="Title")
     * @FOSRest\RequestParam(name="email", description="Email")
     * @FOSRest\RequestParam(name="password", description="Password")
     */
    public function createMaster(ParamFetcherInterface $params): View
    {
        $user = $this->service->createMaster(new RestParamsUserBuilder($params));

        return View::create($user, Response::HTTP_CREATED)->setContext(
            (new Context())
                ->addGroup('id')
                ->addGroup('user')
        );
    }

    /**
     * @FOSRest\Post("/slave")
     *
     * @FOSRest\RequestParam(name="title", description="Title")
     * @FOSRest\RequestParam(name="email", description="Email")
     * @FOSRest\RequestParam(name="password", description="Password")
     */
    public function createSlave(UserInterface $user, ParamFetcherInterface $params): View
    {
        /** @var User $user */
        $user = $this->service->createSlave($user->id(), new RestParamsUserBuilder($params));

        return View::create($user, Response::HTTP_CREATED)->setContext(
            (new Context())
                ->addGroup('id')
                ->addGroup('user')
        );
    }
}
