<?php

declare(strict_types=1);

namespace Sky\Application\Handler\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\JsonSerializationVisitor;
use Sky\Component\Core\Exception\ValidationException;

class ValidationExceptionHandler
{
    public function serialize(JsonSerializationVisitor $visitor, ValidationException $exception, array $type, Context $context)
    {
        return $visitor->visitArray(iterator_to_array($exception->violations()), $type, $context);
    }
}
