<?php

declare(strict_types=1);

namespace Sky\Application\Console\User;

use Sky\Component\User\Model\Builder\User\ConsoleUserBuilder;
use Sky\Component\User\Repository\UserRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class CreateCommand extends Command
{
    private $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    protected function configure()
    {
        $this->setName('sky:cron-api:user:create')
            ->addOption('id', null, InputOption::VALUE_REQUIRED)
            ->addOption('title', null, InputOption::VALUE_REQUIRED)
            ->addOption('email', null, InputOption::VALUE_REQUIRED)
            ->addOption('password', null, InputOption::VALUE_REQUIRED)
            ->addOption('parent-id', null, InputOption::VALUE_OPTIONAL)
            ->addOption('roles', null, InputOption::VALUE_OPTIONAL)
            ->addOption('api-key', null, InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Create User');

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $user = (new ConsoleUserBuilder($input))->build();

        $this->repository->save($user);

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }
}
