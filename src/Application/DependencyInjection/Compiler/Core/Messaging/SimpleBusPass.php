<?php

declare(strict_types=1);

namespace Sky\Application\DependencyInjection\Compiler\Core\Messaging;

use Sky\Component\Core\Messaging\RabbitMqMessageConsumer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SimpleBusPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ('test' === $container->getParameter('kernel.environment')) {
            return;
        }

        $container
            ->getDefinition('simple_bus.rabbit_mq_bundle_bridge.commands_consumer')
            ->setClass(RabbitMqMessageConsumer::class);
        $container
            ->getDefinition('simple_bus.rabbit_mq_bundle_bridge.events_consumer')
            ->setClass(RabbitMqMessageConsumer::class);

        $this->configureCommands($container);
        $this->configureEvents($container);
    }

    private function configureCommands(ContainerBuilder $container)
    {
        $asyncCommandsMap = $container->getParameter('core.messaging.async_commands');

        $allHandlers = $container
            ->getDefinition('simple_bus.command_bus.command_handler_map')
            ->getArgument(0);
        $container
            ->getDefinition('simple_bus.asynchronous.command_bus.command_handler_map')
            ->replaceArgument(0, array_intersect_key($allHandlers, $asyncCommandsMap));
        $container
            ->getDefinition('simple_bus.command_bus.command_handler_map')
            ->replaceArgument(0, array_diff_key($allHandlers, $asyncCommandsMap));
    }

    private function configureEvents(ContainerBuilder $container)
    {
        $asyncEventsMap = $container->getParameter('core.messaging.async_events');

        $allSubscribers = $container
            ->getDefinition('simple_bus.event_bus.event_subscribers_collection')
            ->getArgument(0);
        $container
            ->getDefinition('simple_bus.asynchronous.event_bus.event_subscribers_collection')
            ->replaceArgument(0, array_intersect_key($allSubscribers, $asyncEventsMap));
        $container
            ->getDefinition('simple_bus.event_bus.event_subscribers_collection')
            ->replaceArgument(0, array_diff_key($allSubscribers, $asyncEventsMap));

        $container
            ->getDefinition('simple_bus.asynchronous.publishes_predefined_messages_middleware')
            ->replaceArgument(2, array_keys($asyncEventsMap));
    }
}
