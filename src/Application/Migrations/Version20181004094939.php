<?php

declare(strict_types=1);

namespace Sky\Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181004094939 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE confirmation_notifications (notification_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', confirmation_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_7C25F9B5EF1A9D84 (notification_id), INDEX IDX_7C25F9B56BACE54E (confirmation_id), PRIMARY KEY(notification_id, confirmation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', text LONGTEXT NOT NULL, channel SMALLINT NOT NULL COMMENT \'(DC2Type:notification_channel)\', status SMALLINT NOT NULL COMMENT \'(DC2Type:notification_status)\', created_at DATETIME NOT NULL, sent_at DATETIME DEFAULT NULL, delivered_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE confirmation_notifications ADD CONSTRAINT FK_7C25F9B5EF1A9D84 FOREIGN KEY (notification_id) REFERENCES confirmation (id)');
        $this->addSql('ALTER TABLE confirmation_notifications ADD CONSTRAINT FK_7C25F9B56BACE54E FOREIGN KEY (confirmation_id) REFERENCES notification (id)');
        $this->addSql('CREATE INDEX code ON confirmation (code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE confirmation_notifications DROP FOREIGN KEY FK_7C25F9B56BACE54E');
        $this->addSql('DROP TABLE confirmation_notifications');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP INDEX code ON confirmation');
    }
}
