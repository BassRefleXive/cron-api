function _send_phone_ajax(b, c) {
    b.open("GET", c, true);
    b.setRequestHeader("Accept", "message/x-ajax");
    b.onreadystatechange = function() {
        if (b.readyState != 4) {
            return
        }
        cmd = b.responseText;
        if (cmd) {
            cmd = cmd.split("\t");
            if (cmd[0] == "w" && cmd[1]) {
                _alert(JS_GLOBAL_DATA["w" + cmd[1]]);
                return
            } else {
                if (cmd[0] == "phone_code") {
                    show_phone_code_form(0, cmd[1]);
                    return
                } else {
                    if (cmd[0] == "show_phone_code_form") {
                        show_phone_code_form(1, cmd[1])
                    }
                }
            }
            for (i in cmd) {
                data = cmd[i].split("|");
                print_phone(gpzd(data[1], data[2]), data[0], MOBILE)
            }
            _show_phone(2)
        }
    }
    ;
    b.send("")
}

function show_phone_code_form(d, b) {
    var c = "";
    var f = el("contacts_js");
    if (!f || !f.src) {
        return
    }
    if (!d) {
        c += JS_GLOBAL_DATA.w73;
        c += "<br><br><table align=center border=0 cellpadding=3 cellspacing=0><tr>";
        c += '<td valign=top><img id="ss_tcode_img" onclick="reload_img_src(this,\'/pic.php?mode=ph&type=c&\');" src="/pic.php?mode=ph&type=c&' + escape(Math.random()) + '" vspace=0 style="border:1px black solid;" width=100 height=25></td>';
        c += '<td valign=top style="padding-bottom:5px;"><input type=text value="" id="ads_show_phone" style="width:50px;" maxlength=3></td></tr>';
        c += "</table>";
        c += '<br><center><input type=button class=btn value="' + JS_GLOBAL_DATA.w214 + "\" onclick=\"_show_phone(1, 'code='+encodeURIComponent( el('ads_show_phone').value ), event);return false;\"></center>"
    } else {
        load_script_async("https://www.google.com/recaptcha/api.js?hl=" + b + "&onload=recaptchanloadCallback&render=explicit");
        c += "<div align=center>";
        if (JS_GLOBAL_DATA.w265) {
            c += JS_GLOBAL_DATA.w265
        }
        c += '<div id="recaptcha_html_element" ctype="phone"></div>';
        c += "</div>"
    }
    win(c, JS_GLOBAL_DATA.w220, 0, false, "recaptcha")
}

function _get_phone_key(b) {
    if (!b) {
        return "&e=0"
    }
    return "&x=" + parseInt(b.clientX) + "&y=" + parseInt(b.clientY)
}

function gpzd(data, key) {
    key = key * 6 - 47289 + 517;
    return _ph_dec(data, new String(key), 2)
}

function _ph_dec(g, r, k) {
    g = unescape(_b64_dec(g));
    var n = r.length;
    var d = g.length;
    var c = "";
    var q, p;
    for (var f = 0; f < d; f++) {
        q = g.substring(f, f + 1);
        p = r.substring(f % n, f % n + 1);
        if (k == 1) {
            q = q.charCodeAt(0) - p.charCodeAt(0)
        } else {
            if (k == 2) {
                q = q.charCodeAt(0) - p.charCodeAt(0) + 14
            } else {
                q = q.charCodeAt(0) ^ p.charCodeAt(0)
            }
        }
        c = c + String.fromCharCode(q)
    }
    return c
}

function _b64_dec(n) {
    var f = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var d, c, b, s, r, q, p, u, k = 0, g = "";
    do {
        s = f.indexOf(n.charAt(k++));
        r = f.indexOf(n.charAt(k++));
        q = f.indexOf(n.charAt(k++));
        p = f.indexOf(n.charAt(k++));
        u = s << 18 | r << 12 | q << 6 | p;
        d = u >> 16 & 255;
        c = u >> 8 & 255;
        b = u & 255;
        if (q == 64) {
            g += String.fromCharCode(d)
        } else {
            if (p == 64) {
                g += String.fromCharCode(d, c)
            } else {
                g += String.fromCharCode(d, c, b)
            }
        }
    } while (k < n.length);return g
}