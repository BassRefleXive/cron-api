<?php
/**
 * Created by Michael Bagrov
 * Date: 6/19/15
 * Time: 12:04 PM
 */

error_reporting(E_ALL);
date_default_timezone_set('Europe/Riga');

function _b64_dec($h)
{
    $e = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    $g = 0;
    $len = strlen($h);
    $f = '';
    do {
        $test1 = substr($h, $g++, 1);
        $test2 = substr($h, $g++, 1);
        $test3 = substr($h, $g++, 1);
        $test4 = substr($h, $g++, 1);
        $q = strpos($e, $test1);
        $p = strpos($e, $test2);
        $n = strpos($e, $test3);
        $k = strpos($e, $test4);
        $r = $q << 18 | $p << 12 | $n << 6 | $k;
        $d = $r >> 16 & 255;
        $c = $r >> 8 & 255;
        $b = $r & 255;
        if ($n === 64) {
            $f .= chr($d);
        } else {
            if ($k === 64) {
                $f .= chr($d) . chr($c);
            } else {
                $f .= chr($d) . chr($c) . chr($b);
            }
        }
    } while ($g < $len);
    return $f;
}

function _ph_dec($f, $p, $g)
{
    $f = rawurldecode(_b64_dec($f));
    $h = strlen($p);
    $d = strlen($f);
    $c = '';
    for ($e = 0; $e < $d; $e++) {
        $n = substr($f, $e, 1);
        $k = substr($p, $e % $h, $e % $h + 1);
        if ($g === 2) {
            $n = ord(substr($n, 0, 1)) - ord(substr($k, 0, 1)) + 14;
        } else {
            $n = ord(substr($n, 0, 1)) ^ ord(substr($k, 0, 1));
        }
        $c .= chr($n);
    }
    return $c;
}

function gpzd($data, $key)
{
    $key = $key * 6 - 47289 + 517;
    $ret = _ph_dec($data, (string)$key, 2);

    return $ret;
}

function file_get_contents_curl($url, array $aHeaders = [])
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

    curl_setopt($ch, CURLOPT_URL, $url);

    if (sizeof($aHeaders)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeaders);
    }

    $data = curl_exec($ch);

    $sContent = substr($data, curl_getinfo($ch, CURLINFO_HEADER_SIZE));

    curl_close($ch);

    return $sContent;
}

$aLinks = [
    'https://www.ss.com/msg/ru/transport/cars/bmw/750/elicl.html',
];

function getPhone($scriptSrc) {
//    $phoneKey = '&e=0';
//    $phoneKey = sprintf('&x=%d&y=%d', 520, 309);
}

function print_phone($c, $g) {

}


//1|JTg3JTVDJTk2JTkzeGl2JTkzc2pxJTVCcSU3RCU5MW1xeCU5MCU2MCU4OCU5MCU3RVp3anlaeXhkZg==|65936754
//2|JTg2JTkxbyU4RnJuJTdDJTk2c2lrWg==|55694159

$encodedPhones = [
    [
        1,
        'JTg3JTVDJTk2JTkzeGl2JTkzc2pxJTVCcSU3RCU5MW1xeCU5MCU2MCU4OCU5MCU3RVp3anlaeXhkZg==',
        65936754,
    ],
    [
        2,
        'JTg2JTkxbyU4RnJuJTdDJTk2c2lrWg==',
        55694159,
    ],
    [
        3,
        'JTg3VCU5MSU4RnpscSU4RnNqaVZtJTdGJTk0aG14JTkwWCU4MyU4QyU4MCU1RHJmeVpxcyU2MGg=',
        51707164,
    ],
    [
        4,
        'JTg4VSU5OSU5M3FncSU5MXFraiU1RXF2JThGaG92JTkxWSU4QiU5MHdYcmh3JTVCciU3QmRf',
        71424667,
    ],
];
//https://www.ss.com/pic.php?mode=ph&type=c
foreach ($encodedPhones as $encodedPhone) {
    $phone = _ph_dec(gpzd($encodedPhone[1], $encodedPhone[2]), 'K0dbVwzGrpLa-wRs2', 2);

    echo implode('', explode('-', $phone)) . PHP_EOL;
}



//
//$phone = implode('', explode('-', $tmpProne));

//echo $phone . PHP_EOL;

//foreach ($aLinks as $url) {
//    $response = file_get_contents_curl($url);
//
//    file_put_contents('ad.html', $response);
//
//    if (preg_match('/<script async id="contacts_js" src="(?<contacts_js>.*?)"/', $response, $matches)) {
//        $requestUri = 'http://www.ss.lv' . $matches['contacts_js'];
//
//        $response2 = file_get_contents_curl($requestUri);
//
//        file_put_contents('response2.txt', $response2);
//
////        if (preg_match_all('/"+[a-zA-Z0-9\+\/\=]+","+[a-zA-Z0-9\+\/\=]+"/', $response2, $matches)) {
////
////            foreach ($matches[0] as $match) {
////                $parts = explode(',', $match);
////
////                $data = substr($parts[0], 1, strlen($parts[0]) - 2);
////
////                $key = substr($parts[1], 1, strlen($parts[1]) - 2);
////
////                $tmpProne = _ph_dec(gpzd($data, $key), $c, 2);
////
////                echo $tmpProne . PHP_EOL;
////
////                $phone = implode('', explode('-', $tmpProne));
////
////                echo $phone . PHP_EOL;
////            }
////
////        }
//
//    }
//
//}
